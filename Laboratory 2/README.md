# Power attacks.

Each task contains a trace package and a loader Jupyter Notebook. Solutions are to be delivered as Jupyter Notebooks.

## Task 1.

We suspect SimpleBank's CTO has been in contact with the competition. We have collected his cellphone radio emmissions. The signal is too noisy to hope for any data recovery, but we would like you to find out whether he has been making calls at regular intervals. This should be enough to blackmail him into cooperation. Test if the signal is periodic. Find the period and number of repetitions.
## Task 2.

SimpleBank uses SimpleSafe technology to protect its safes. Our agent has been collecting power traces of one of their safes while attempting to brute-force the password, but he had to abort the mission just after testing a few dozen combinations. See what you can dig out of the traces.

## Task 3.

SimpleBank's CEO uses AES encryption to secure his phone calls. We have been monitoring his cell phone's power consumption for some time now. Crack his AES key using the power traces we collected.

## Task 4.

SimpleBank uses SimpleAESVault technology to secure its main vault. Our agent, posing as a cleaner, can gain access to the vault room for a short period, but nowhere near enough to mount an attack from scratch on site. We have bought a SimpleAESVault ourselves for analysis, we would like you to prepare an attack that can be executed on site in minimum time (number of traces). Preparation time is unimportant. Crack AES key using less than `N` traces.