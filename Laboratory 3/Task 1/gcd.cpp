#include <iostream>
#include <stdint.h>

static int64_t p = 41;
static int64_t q = 71;

int64_t gcd(int64_t p, int64_t q)
{
    return q == 0 ? p : gcd(q, p % q);
}
  
int main(int argc, char* argv[])
{
    int64_t n = p * q;
    int64_t m = atoi(argv[1]);
    int64_t mf = atoi(argv[2]);
    
    if (m > mf)
    {
        int64_t x = mf;
        mf = m;
        m  = x;
    }
  
    int64_t f1 = gcd(mf - m, n);
    int64_t f2 = n / f1;
  
    std::cout << f1 << ", " << f2 << std::endl;
}