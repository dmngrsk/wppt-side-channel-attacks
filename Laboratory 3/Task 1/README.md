Usage:

- `make`
- `make decrypt` (denote result value as `$1`)
- `make fault` (denote result value as `$2`)
- `make gcd M=$1 MF=$2`