#include <iostream>
#include <stdint.h>

static int64_t p  = 41;
static int64_t q  = 71;

int64_t mod(int64_t x,int64_t n)
{
    return x % n;
}

int64_t pow(int64_t x, int64_t p, int64_t n)
{
    if (p == 0) return 1;
    if (p == 1) return x;
    int64_t tmp = pow(x, p/2, n);
    tmp = mod(tmp*tmp,n);
  
    return p % 2 == 0 ? tmp : mod(tmp * x, n);
}


int64_t gcd(int64_t p, int64_t q)
{
    return q == 0 ? p : gcd(q, p % q);
}

int64_t lcm(int64_t p,int64_t q)
{
    return p * q / gcd(p,q);
}

void egcd(int64_t e, int64_t phi, int64_t &g, int64_t &y, int64_t &x)
{
    if (e == 0) 
    {
        g = phi;
        y = 0;
        x = 1;
        return;
    }

    egcd(phi%e,e,g,y,x);
    int64_t a = y;
    y = x - (phi / e) * y;
    x = a;
}

int64_t modinv(int64_t e, int64_t phi)
{
    int64_t g, x, y;
    egcd(e, phi, g, x, y);
    return mod(x, phi);
}

int64_t crt(int64_t m1, int64_t m2, int64_t p, int64_t q, int64_t qi)
{
    int64_t h  = (qi * (m1 - m2)) % p;
    return m2 + h * q;
}

int64_t dec(int64_t c, int64_t p, int64_t q, int64_t dp, int64_t dq, int64_t qi)
{
    int64_t m1 = pow(c, dp, p);
    int64_t m2 = pow(c, dq, q);
    return crt(m1, m2, p, q, qi);
}

int main(void)
{
    int64_t e  = 3;
    int64_t c  = 230;
    int64_t n  = p*q;
  
    int64_t d  = modinv(e, lcm(p - 1, q - 1));
    int64_t dp = mod(d, p - 1);
    int64_t dq = mod(d, q - 1);
    int64_t qi = modinv(q, p);
  
    int64_t m  = dec(c, p, q, dp, dq, qi);
  
    std::cout << "m = " << m  << std::endl;
}