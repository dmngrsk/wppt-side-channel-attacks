set pagination off
# Attack 5a
# Change the OR instruction from expression "c |= *p++ ^ *q++;" to an instruction that does nothing.

break *0x004005e3
run
set *(char*)0x004005e3 ^= (1 << 1)
delete 1
continue
quit
