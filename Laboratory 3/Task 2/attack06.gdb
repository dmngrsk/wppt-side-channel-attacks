set pagination off
# Attack 6
# Swap the order of operands in the "return c;" expression (for some reason it is done with a single bit flip).

break *0x004005f3
run
set *(char*)0x004005f3 ^= (1 << 1)
continue
quit