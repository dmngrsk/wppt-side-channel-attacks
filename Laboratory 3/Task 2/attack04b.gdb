set pagination off
# Attack 4b
# Change the instruction test %eax,%eax to an instruction that does not set the ZF flag.

break *0x004005ef
run
set *(char*)0x004005ef ^= (1 << 1)
continue
quit
