set pagination off
# Attack 2b
# Manipulate the program counter in order to skip the password validation step in test_pass.

break *0x004005e3
run
set $pc = ((uint32_t)$pc) ^ (1 << 4)
continue
quit