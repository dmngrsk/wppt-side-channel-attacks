set pagination off
# Attack 4a
# Change the instruction test %eax,%eax to an instruction that does not set the ZF flag.

break *0x00400615
run
set *(char*)0x00400615 ^= (1 << 1)
continue
quit