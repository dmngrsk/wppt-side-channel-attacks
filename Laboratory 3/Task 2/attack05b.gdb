set pagination off
# Attack 5b
# Change the OR instruction from expression "c |= *p++ ^ *q++;" to an instruction that does nothing.

break *0x004005e3
run
set *(char*)0x004005e3 ^= (1 << 6)
delete 1
continue
quit
