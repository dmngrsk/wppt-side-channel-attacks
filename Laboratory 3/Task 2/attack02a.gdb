set pagination off
# Attack 2a
# Manipulate the program counter in order to skip the password validation step in test_pass.

break *0x004005f1
run
set $pc = ((uint32_t)$pc) ^ (1 << 1)
continue
quit