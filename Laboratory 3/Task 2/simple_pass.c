#include <stdio.h>
#include <string.h>

char secr[] = "verysafe";

unsigned test_pass(char *p, char *q)
{
    unsigned n = strlen(p);
    if (strlen(q) != n) return 1;
    
    unsigned c = 0;
    while (n--)
    {
        c |= *p++ ^ *q++;
    }

    return c;
}

void run_simple_pass(char *p)
{
    if (test_pass(p, secr) == 0)
    {
        printf("Pass\n");
    }
    else 
    {
        printf("Fail\n");
    }
}

int main(int argc, char *argv[])
{
    if (argc < 2) return 1;

    run_simple_pass(argv[1]);
    return 0;
}