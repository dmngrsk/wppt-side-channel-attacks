Usage:

- `make`
- `make faults` (it will save the fault samples to `faults.txt`)
- `make k10` (denote result key as `$1`)
- `make key K10=$1`