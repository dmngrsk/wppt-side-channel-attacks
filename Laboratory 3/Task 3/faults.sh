#!/usr/bin/env bash
touch faults.gdb

echo "set pagination off" >> faults.gdb
echo "" >> faults.gdb
echo "# Break between 8th and 9th MixColumns call." >> faults.gdb
echo "break aes.c:401 if round = 9" >> faults.gdb
echo "" >> faults.gdb
echo "# Original cipher, required for r10.py" >> faults.gdb
echo "run" >> faults.gdb
echo "continue" >> faults.gdb
echo "" >> faults.gdb
echo "# All possible faults. Note that this is a silly man's way of getting the faulted samples." >> faults.gdb
echo "# Can't be amused to do anything smarter (analyzing which 4-5 state byte changes yield which difference bytes," >> faults.gdb
echo "# limitting the amount of state value changes, etc.) as it would require writing some extra code." >> faults.gdb

for j in {0..3}
do
	for i in {0..3}
	do
		for b in {0..255}
		do
			echo "run" >> faults.gdb
			echo "set variable (*state)[$j][$i] = $b" >> faults.gdb
			echo "continue" >> faults.gdb
		done
	done
done
