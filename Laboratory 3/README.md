# Fault injection attacks.

Attacks are to be implemented in simulation using `gdb` (or `dynamorio`) on x86/x86_64. You can use any operating system you want. Solutions may be delivered face-to-face on lecture or by email as an archive containing: { complete source code, build/execution scripts, final binary, attack screenshots + commentary }.

## Task 1.

Replicate the [Bellcore attack](https://eprint.iacr.org/2012/553.pdf) on an x86 implementation of RSA-CRT. You can use any RSA-CRT implementation you find. Attack is considered successful when you recover the factors of modulus `N`.

## Task 2.

Use single-bit-flip faults to make the password check pass on incorrect password. Attack is considered successful when program prints `Pass` and does not print `Fail` on all incorrect passwords of the same length as the secret. It doesn't matter if the program crashes afterwards. You can only attack `run_simple_pass` or `test_pass` functions. You cannot attack `main`, startup scripts etc. Other than that, you can target anything you want: program counter, registers, flags, stack, data/instructions in memory...

## Task 3. 

Replicate [Differential Fault Analysis on AES](https://eprint.iacr.org/2003/010.pdf). Attack is considered successful when you recover the entire key. You can use any AES implementation, eg. [tiny-AES128-C](https://github.com/bitdust/tiny-AES128-C). Hints:
- Round 8 / Round 9 faults between 3rd and 4th MixColumns
- https://github.com/SideChannelMarvels/JeanGrey
- https://github.com/SideChannelMarvels/Stark
