module Laboratory.Core.Strategies.SelectorOne

open Laboratory.Core.Utilities
open Laboratory.Infrastructure
open System.Diagnostics

let executeSelectorOneStrategy(client:TcpServerClient) = 

    let mutable acc = Array.empty<byte>
    let mutable passwordFound = false

    while not passwordFound do
        for i in 0x00..0xFF do
            if not passwordFound then do
                let challenge = Array.concat [| acc; [| (byte i) |] |]
                let (isValid, time) = measureTime (fun () -> client.VerifyPassword(challenge, 1))
                if (float client.TimeoutDuration) > time && isValid then do passwordFound <- true
                if (float client.TimeoutDuration) < time || isValid then do acc <- challenge

    acc