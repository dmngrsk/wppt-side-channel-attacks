module Laboratory.Core.Strategies.SelectorFour

open Laboratory.Core.Utilities
open Laboratory.Infrastructure
open System.Diagnostics

let executeSelectorFourStrategy(client:TcpServerClient) = 
    
    let maxLength = 6
    let mutable acc = Array.empty<byte>
    
    let measureOne b bs =
        let (isValid, time) = measureTime (fun () -> client.VerifyPasswordWithDelay(bs, 4, 500))
        // printfn "%A: %A" bs time
        (b, isValid, time)

    let measureMany cnt i b =
        Array.concat [| acc; [| b |]; Array.zeroCreate (maxLength - i - 1) |]
        |> Array.create cnt
        |> Array.map (measureOne b)
        |> Array.minBy trd3

    while Array.length acc < maxLength do
        let (b, isValid, time) =
            [| 0x00 .. 0xFF |]
            |> Array.map byte
            |> Array.map (measureMany 10 (Array.length acc))
            |> Array.maxBy trd3
        acc <- Array.append acc [| b |]

    let passwordFound = client.VerifyPassword(acc, 4)
    match passwordFound with
    | true -> acc
    | false -> failwith "Failed to find the password."