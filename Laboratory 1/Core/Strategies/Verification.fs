module Laboratory.Core.Strategies.Verification

open Laboratory.Core.Utilities
open Laboratory.Infrastructure
open System.Diagnostics

let executeVerificationStrategy(client:TcpServerClient) = 

    let passwordOne = Array.map byte [| 0xF0; 0x02; 0xDC; 0x09; 0x12; 0xDC |]
    let passwordTwo = Array.map byte [| 0x95; 0x2C; 0x70; 0x48; 0xA9; 0xD7 |]
    let passwordThree = Array.map byte [| 0xED; 0x64; 0x05; 0xAE; 0x47; 0xB3 |]
    let passwordFour = Array.map byte [| 0x9F; 0xCC; 0x2D; 0xBF; 0x81; 0xA7 |]

    printfn "S1: %A -> %A" passwordOne (client.VerifyPassword(passwordOne, 1))
    printfn "S2: %A -> %A" passwordTwo (client.VerifyPassword(passwordTwo, 2))
    printfn "S3: %A -> %A" passwordThree (client.VerifyPassword(passwordThree, 3))
    printfn "S4: %A -> %A" passwordFour (client.VerifyPassword(passwordFour, 4))

    Array.create 6 (byte 0)