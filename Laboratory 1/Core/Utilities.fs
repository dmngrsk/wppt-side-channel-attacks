module Laboratory.Core.Utilities

open System
open System.Diagnostics
open System.Threading

let fst3 (a, b, c) = a
let snd3 (a, b, c) = b
let trd3 (a, b, c) = c

let parseByteArray(bytes:byte[]) =
    (bytes |> Array.map (fun x -> x.ToString()) |> String.concat " ")

let measureTime f =
    let timer = new Stopwatch()
    timer.Start()
    let result = f()
    (result, timer.Elapsed.TotalMilliseconds)