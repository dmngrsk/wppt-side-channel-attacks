module Laboratory.CliParser

open Argu

type CliArguments =
    | [<ExactlyOnce; AltCommandLine("-h")>] Host of host:string
    | [<ExactlyOnce; AltCommandLine("-p")>] Port of port:int
    | [<ExactlyOnce; AltCommandLine("-i")>] Index of index:int
    | [<ExactlyOnce; AltCommandLine("-s")>] Selector of num:int
    | Tcp_Debug
with
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | Host _ -> "server host address."
            | Port _ -> "server host port."
            | Index _ -> "your index number."
            | Selector _ -> "selector of the function to crack."
            | Tcp_Debug _ -> "write debug information about the TCP transfer."

let parser = ArgumentParser.Create<CliArguments>(programName = "timing-attack")