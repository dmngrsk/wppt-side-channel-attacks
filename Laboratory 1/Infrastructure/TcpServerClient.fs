namespace Laboratory.Infrastructure

open Laboratory.Core.Utilities
open System
open System.Threading
open System.Threading.Tasks
open System.Net.Sockets

type TcpServerClient(host:string, port:int, debug:bool) = 

    let innerClient = new TcpClient(host, port)
    let inputStream = innerClient.GetStream()
    let outputStream = innerClient.GetStream()
    let mutable indexNumberSet:bool = false
    
    member this.TimeoutDuration = 5000

    member this.SetIndexNumber(number:int) =
        match indexNumberSet with
        | true -> failwith "Index number is already set."
        | false -> 
            let indexNumber = BitConverter.GetBytes(number)
            inputStream.Write(indexNumber, 0, (Array.length indexNumber))
            inputStream.Flush() 
            indexNumberSet <- true

    member this.VerifyPassword(password:byte[], selector:int) =
        this.WriteBytes([| (byte selector) |])
        this.WriteBytes(password)
        let response = this.ReadBytes()
        response |> Array.head = (byte 1)

    member this.VerifyPasswordWithDelay(password:byte[], selector:int, delay:int) =
        this.WriteBytes([| (byte selector) |])
        this.WriteBytes([| Array.head password |])
        Thread.Sleep(delay)
        this.WriteBytes(Array.tail password)
        let response = this.ReadBytes()
        response |> Array.head = (byte 1)

    member private this.WriteBytes(bytes:byte[]) =
        inputStream.Write(bytes, 0, (Array.length bytes))
        inputStream.Flush()
        if debug then printfn ">> %s" (parseByteArray bytes)

    member private this.ReadBytes() =
        let response = Array.zeroCreate 1
        let cts = new CancellationTokenSource()

        let workflow = async {
            let tasks =
                [ async {
                    outputStream.Read(response, 0, 1) |> ignore
                    if debug then printfn "<< %s" (parseByteArray response) };
                  async {
                    do! Async.Sleep(this.TimeoutDuration)
                    let! token = Async.CancellationToken
                    while not token.IsCancellationRequested do
                        this.WriteBytes([| (byte 0) |])
                        inputStream.Flush()
                        do! Async.Sleep(1000) } ]
                |> Seq.map (fun x -> Async.StartAsTask(x, cancellationToken = cts.Token))
                |> Task.WhenAny

            tasks.Result |> ignore
        }

        workflow |> Async.RunSynchronously |> ignore
        cts.Cancel()
        response