﻿module Laboratory.Program

open Argu
open Laboratory.CliParser
open Laboratory.Core
open Laboratory.Core.Strategies.SelectorOne
open Laboratory.Core.Strategies.SelectorTwo
open Laboratory.Core.Strategies.SelectorThree
open Laboratory.Core.Strategies.SelectorFour
open Laboratory.Core.Strategies.Verification
open Laboratory.Core.Utilities
open Laboratory.Infrastructure
open System

[<EntryPoint>]
let main argv =
    try
        let arguments = parser.ParseCommandLine argv
        let host = arguments.GetResult CliArguments.Host
        let port = arguments.GetResult CliArguments.Port
        let indexNumber = arguments.GetResult CliArguments.Index
        let selector = arguments.GetResult CliArguments.Selector
        let debug = arguments.Contains CliArguments.Tcp_Debug

        let client = new TcpServerClient(host, port, debug)
        client.SetIndexNumber(indexNumber)

        let password = 
            match selector with
            | 0 -> executeVerificationStrategy(client)
            | 1 -> executeSelectorOneStrategy(client)
            | 2 -> executeSelectorTwoStrategy(client)
            | 3 -> executeSelectorThreeStrategy(client)
            | 4 -> executeSelectorFourStrategy(client)
            | _ -> failwith "Invalid selector given."

        printfn "Password for selector %d: %s" selector (parseByteArray password)

    with :? ArguParseException as e -> printfn "%s" e.Message

    0
