# Timing attacks.

Attack a TCP server.

Once you connect to the server, you need to pass in your index number (4 bytes) in order to authenticate. Then, you are able to send challenges of the following form: a function selector (1 byte), then a password of your choice (length to be discovered). If the password was correct, the server will send back a 1-byte status value equal to 1, otherwise it will send a 1-byte state value equal to 0.

- Crack the password behind function 1 (selector byte equal to 1).
- Crack the password behind function 2 (selector byte equal to 2).
- Crack the password behind function 3 (selector byte equal to 3).
- Crack the password behind function 4 (selector byte equal to 4).
